import React, { Component } from "react";
import Header from "../../components/header/index";
import {
  Box,
  Button,
  Container,
  Typography,
  withStyles,
} from "@material-ui/core";
import { NavLink } from "react-router-dom";
import styles from "./style";
class Home extends Component {
  render() {
    const { title, navLink, button } = this.props.classes;
    return (
      <div>
        <Header />
        <Container maxWidth="lg">
          <Typography className={title} align="center" variant="h3">
            Welcome to Testing Online
          </Typography>
          <NavLink className={navLink} to="/multiplechoice">
            <Box align="center">
              <Button className={button} variant="contained" color="primary">
                GET STARTED
              </Button>
            </Box>
          </NavLink>
        </Container>
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(Home);
