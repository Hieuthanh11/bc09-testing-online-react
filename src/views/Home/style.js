const styles = (theme) => {
  return {
    title: {
      margin: "40px 0px",
    },
    navLink: {
      textDecoration: "none",
    },
    button: {
      fontSize: 30,
    },
  };
};

export default styles;
