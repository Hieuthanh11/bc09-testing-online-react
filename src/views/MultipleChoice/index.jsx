import {
  Box,
  Button,
  Container,
  FormControl,
  FormControlLabel,
  FormLabel,
  Radio,
  RadioGroup,
  Typography,
  withStyles,
} from "@material-ui/core";
import React, { Component } from "react";
import { connect } from "react-redux";
import { createAction } from "../../store/action";
import { fetchQuestion } from "../../store/action/question";
import { actionType } from "../../store/action/type";
import Header from "../../components/header";
import { NavLink } from "react-router-dom";
import styles from "./style";
class MultipleChoice extends Component {
  handleChange = (event) => {
    const values = event.target.value.split("-");
    console.log(values);
    if (values[0] === "true") {
      let correctAnswer = {
        questionId: values[2],
        answer: {
          content: values[1],
          exact: values[0],
        },
      };
      this.props.answersList.push(correctAnswer);
    }
  };

  handleSubmit = (event) => {
    event.preventDefault();
    this.props.dispatch(
      createAction(actionType.ADD_ANSWERS, this.props.answersList)
    );
  };

  render() {
    console.log(this.props.answersList);
    const { navLink, button } = this.props.classes;
    return (
      <div>
        <Header />
        <Container>
          <Box marginY="20px">
            <Typography variant="h4" align="center">
              I.Choose the correct answers
            </Typography>
          </Box>
          <form onSubmit={this.handleSubmit}>
            {this.props.questionList.map((item) => {
              const { questionType, id, content, answers } = item;
              if (questionType === 1) {
                return (
                  <div key={id}>
                    <FormControl component="fieldset">
                      <FormLabel component="legend">
                        Câu{id}: {content}
                      </FormLabel>
                      <RadioGroup onChange={this.handleChange}>
                        {answers.map((item) => {
                          return (
                            <FormControlLabel
                              value={item.exact + "-" + item.content + "-" + id}
                              key={item.id}
                              control={<Radio />}
                              label={item.content}
                            />
                          );
                        })}
                      </RadioGroup>
                    </FormControl>
                  </div>
                );
              }
            })}
            <Box marginY="20px" display="flex">
              <div className={button}>
                <Button type="submit" variant="contained" color="primary">
                  Submit
                </Button>
              </div>
              <NavLink className={navLink} to="/fillinblank">
                <Button type="button" variant="contained" color="primary">
                  Next
                </Button>
              </NavLink>
            </Box>
          </form>
        </Container>
      </div>
    );
  }
  async componentDidMount() {
    this.props.dispatch(fetchQuestion);
  }
}
const mapStateToProps = (state) => {
  return {
    questionList: state.question.questionList,
    answersList: state.question.answersList,
  };
};
export default connect(mapStateToProps)(withStyles(styles)(MultipleChoice));
