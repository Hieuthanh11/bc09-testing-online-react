const styles = (theme) => {
  return {
    navLink: {
      textDecoration: "none",
    },
    button: {
      flexGrow: 1,
    },
  };
};

export default styles;
