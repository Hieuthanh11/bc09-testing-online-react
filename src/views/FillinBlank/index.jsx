import {
  FormControl,
  FormLabel,
  Typography,
  TextField,
  Button,
  Box,
  withStyles,
  Container,
} from "@material-ui/core";
import React, { Component } from "react";
import { connect } from "react-redux";
import { createAction } from "../../store/action";
import { fetchQuestion } from "../../store/action/question";
import { actionType } from "../../store/action/type";
import Header from "../../components/header";
import styles from "./style";
import { NavLink } from "react-router-dom";
class FillInBlank extends Component {
  handleChange = (event) => {
    const name = event.target.name.split("-");
    const value = event.target.value;
    console.log(value);
    if (value.toLowerCase() === name[1].toLowerCase()) {
      let correctAnswer = {
        questionId: name[2],
        answer: {
          content: name[1],
          exact: name[0],
        },
      };
      this.props.answersList.push(correctAnswer);
    }
  };

  handleSubmit = (event) => {
    event.preventDefault();
    this.props.dispatch(
      createAction(actionType.ADD_ANSWERS, this.props.answersList)
    );
  };

  render() {
    const { navLink, button, question, answer } = this.props.classes;

    return (
      <div>
        <Header />
        <Container>
          <Box marginY="20px">
            <Typography variant="h4" align="center">
              II.Fill in the blank with correct answers
            </Typography>
          </Box>
          <form onSubmit={this.handleSubmit}>
            {this.props.questionList.map((item) => {
              const { questionType, id, content, answers } = item;
              if (questionType === 2) {
                return (
                  <div key={id}>
                    <FormControl component="fieldset">
                      <FormLabel component="legend">
                        Câu{id}: {content}
                      </FormLabel>
                      {answers.map((item) => {
                        return (
                          <TextField
                            className={answer}
                            key={item.id}
                            name={item.exact + "-" + item.content + "-" + id}
                            onChange={this.handleChange}
                          />
                        );
                      })}
                    </FormControl>
                  </div>
                );
              }
            })}
            <Box marginY="20px" display="flex">
              <div className={button}>
                <Button type="submit" variant="contained" color="primary">
                  Submit
                </Button>
              </div>
              <NavLink className={navLink} to="/result">
                <Button type="button" variant="contained" color="primary">
                  Next
                </Button>
              </NavLink>
            </Box>
          </form>
        </Container>
      </div>
    );
  }
  async componentDidMount() {
    this.props.dispatch(fetchQuestion);
  }
}

const mapStateToProps = (state) => {
  return {
    questionList: state.question.questionList,
    answersList: state.question.answersList,
  };
};

export default connect(mapStateToProps)(withStyles(styles)(FillInBlank));
