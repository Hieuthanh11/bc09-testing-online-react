const styles = (theme) => {
  return {
    input: {
      marginY: 20,
    },
    navLink: {
      textDecoration: "none",
    },
    button: {
      flexGrow: 1,
    },
    answer: {
      margin: "20px 0",
    },
  };
};

export default styles;
