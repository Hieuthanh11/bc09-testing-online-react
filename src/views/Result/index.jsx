import {
  Box,
  Button,
  Table,
  TableContainer,
  TableHead,
  TableRow,
  TableCell,
  Typography,
  withStyles,
  TableBody,
  Container,
} from "@material-ui/core";
import React, { Component } from "react";
import { connect } from "react-redux";
import Header from "../../components/header";
import styles from "./style";
class Result extends Component {
  constructor(props) {
    super(props);
    this.state = {
      flag: false,
    };
  }

  showAnswers = () => {
    this.setState({
      flag: true,
    });
  };

  renderAnswers = () => {
    const { answersList } = this.props;
    return answersList.map((item, index) => {
      return (
        <TableRow key={index}>
          <TableCell>{item.questionId}</TableCell>
          <TableCell>{item.answer.content}</TableCell>
        </TableRow>
      );
    });
  };

  render() {
    const { button } = this.props.classes;
    return (
      <div>
        <Header />
        <Container>
          <Box marginY="20px">
            <Typography variant="h3" align="center">
              Result
            </Typography>
          </Box>
          <div align="center">
            <Button
              onClick={this.showAnswers}
              type="button"
              className={button}
              variant="contained"
              color="primary"
            >
              Total ({this.props.answersList.length}/
              {this.props.questionList.length})
            </Button>
          </div>
          {this.state.flag ? (
            <div>
              <Container maxWidth="md">
                <TableContainer>
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell>Câu</TableCell>
                        <TableCell>Đáp án</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>{this.renderAnswers()}</TableBody>
                  </Table>
                </TableContainer>
              </Container>
            </div>
          ) : null}
        </Container>
      </div>
    );
  }
}
const mapStateToProp = (state) => {
  return {
    answersList: state.question.answersList,
    questionList: state.question.questionList,
  };
};
export default connect(mapStateToProp)(withStyles(styles)(Result));
