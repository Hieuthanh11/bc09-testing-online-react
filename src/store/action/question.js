import axios from "axios";
import { createAction } from "./index";
import { actionType } from "./type";

export const fetchQuestion = async (dispactch) => {
  try {
    const res = await axios({
      url: "https://5bd2959ac8f9e400130cb7e9.mockapi.io/api/questions",
      method: "GET",
    });
    dispactch(createAction(actionType.FETCH_QUESTION, res.data));
  } catch (err) {
    console.log(err);
  }
};
