import { actionType } from "../action/type";

const inititalState = {
  questionList: [],
  answersList: [],
};

const reducer = (state = inititalState, action) => {
  switch (action.type) {
    case actionType.FETCH_QUESTION: {
      state.questionList = action.payload;
      return { ...state };
    }
    case actionType.ADD_ANSWERS: {
      state.answersList = action.payload;
      return { ...state };
    }
    default:
      return state;
  }
};

export default reducer;
