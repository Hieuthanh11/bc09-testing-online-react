import {
  AppBar,
  IconButton,
  Toolbar,
  Typography,
  withStyles,
} from "@material-ui/core";
import { LocalLibrary } from "@material-ui/icons";
import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import styles from "./style";
class Header extends Component {
  render() {
    const { navLink, active, title } = this.props.classes;
    return (
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" color="inherit" aria-label="menu">
            <LocalLibrary />
          </IconButton>
          <Typography className={title} variant="h6">
            TESTING-ONLINE
          </Typography>
          <NavLink exact className={navLink} activeClassName={active} to="/">
            Home
          </NavLink>
          <NavLink
            className={navLink}
            activeClassName={active}
            to="/multiplechoice"
          >
            MultipleChoice
          </NavLink>
          <NavLink
            className={navLink}
            activeClassName={active}
            to="/fillinblank"
          >
            FillInBlank
          </NavLink>
          <NavLink className={navLink} activeClassName={active} to="/result">
            Result
          </NavLink>
        </Toolbar>
      </AppBar>
    );
  }
}

export default withStyles(styles)(Header);
