const styles = (theme) => {
  return {
    title: {
      flexGrow: 1,
    },
    navLink: {
      textDecoration: "none",
      color: "#ffffff",
      marginRight: 20,
    },
    active: {
      color: "#FFFF66",
    },
  };
};

export default styles;
