import React, { Component } from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import FillinBlank from "./views/FillinBlank";
import Home from "./views/Home";
import MultipleChoice from "./views/MultipleChoice";
import Result from "./views/Result/";
class App extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/fillinblank" component={FillinBlank} />
            <Route path="/multiplechoice" component={MultipleChoice} />
            <Route path="/result" component={Result} />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
